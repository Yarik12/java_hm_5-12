package com.danIT.HW6;

public final class Woman extends Human {

    public Woman() {
    }

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, int iq, Family family, String[][] schedule) {
        super(name, surname, year, iq, family, schedule);
    }

    public Woman(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    public void makeup() {
        System.out.println("Піду підфарбуюся");
    }

    public void greetPet() {
        System.out.println("Привіт, " + this.getFamily().getPet().getNickname());
    }
}
