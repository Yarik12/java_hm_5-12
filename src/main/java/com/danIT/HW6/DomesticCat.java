package com.danIT.HW6;

public class DomesticCat extends Pet implements Foul{


    public DomesticCat() {
    }

    public DomesticCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    public DomesticCat(String nickname, int age) {
        super(nickname, age);
    }

    @Override
    public void foul() {
        System.out.println("Потрібно добре замести сліди...");
    }

    @Override
    public void respond() {
        System.out.println("Привіт, хазяїн. Я - " + this.getNickname() + ". Я скучив!");
    }
}
