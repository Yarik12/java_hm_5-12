package com.danIT.HW6;

public class Dog extends Pet implements Foul{


    public Dog() {
    }

    public Dog(Species dog, String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    public Dog(String nickname, int age) {
        super(nickname, age);
    }

    @Override
    public void foul() {
        System.out.println("Потрібно добре замести сліди...");
    }

    @Override
    public void respond() {
        System.out.println("Привіт, хазяїн. Я - " + this.getNickname() + ". Я скучив!");
    }
}
