package com.danIT.HW6;



public final class Man extends Human {

    public Man() {
    }

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq, Family family, String[][] schedule) {
        super(name, surname, year, iq, family, schedule);
    }

    public Man(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    public void repairCar() {
        System.out.println("Лагоджу авто");
    }

    public void greetPet() {
        System.out.println("Привіт, " + this.getFamily().getPet().getNickname());
    }
}
