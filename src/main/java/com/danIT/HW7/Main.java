package com.danIT.HW7;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Set<String> habits = new HashSet<>();
        habits.add("loves to play with the ball");
        habits.add("loves to chase a stick");

        Map<DayOfWeek, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.MONDAY, "Ldo home work");
        schedule.put(DayOfWeek.TUESDAY, "go to courses; watch a film;");
        schedule.put(DayOfWeek.WEDNESDAY, "pass the English exam;");
        schedule.put(DayOfWeek.THURSDAY, "go to the doctor;");
        schedule.put(DayOfWeek.FRIDAY, "pay for utilities;");
        schedule.put(DayOfWeek.SATURDAY, "write a resume;");
        schedule.put(DayOfWeek.SUNDAY, "go have a beer with friends;");


        Human mother = new Human("Ann", "Melnik", 1990, 100, schedule);
        Human father = new Human("Oleh", "Melnik", 1988, 100, schedule);
        Human child1 = new Human("Ivan", "Melnik", 2010, 100, schedule);
        Human child2 = new Human("Sofi", "Melnik", 2018, 100, schedule);
        Human child3 = new Human("Anton", "Ivanov", 2003, 100, schedule);

        Dog dog = new Dog(Species.DOG, "Barsik", 6, 60, habits);
        Fish fish = new Fish(Species.FISH, "Dori", 1, 10, habits);

        Set<Pet> pets = new HashSet<>();
        pets.add(dog);
        pets.add(fish);

        List<Human> children = new ArrayList<>();
        children.add(child1);
        children.add(child2);
        children.add(child3);


        Family family = new Family(mother, father, children, pets);
        family.addChild(child1);
        family.addChild(child2);
        family.addChild(child3);
        System.out.println(family.getChildren().toString());
        family.deleteChild(child3);
        System.out.println(family.getChildren().toString());

        mother.setFamily(family);
        father.setFamily(family);

        mother.feedPet(true);

       father.feedPet(false);

        mother.greetPet();

        father.describePet();

       family.countFamily();

////        for (int i = 0; i < 100000001; i++) {
////            Human human = new Human();
////        }

      family.bornChild();

        System.out.println(family.getChildren().toString());

        System.out.println(dog.toString());



    }
}
