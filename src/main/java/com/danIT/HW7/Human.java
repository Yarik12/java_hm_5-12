package com.danIT.HW7;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Family family;
    private Map<DayOfWeek, String> schedule;


    public Human() {
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, int iq, Family family, Map<DayOfWeek, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.family = family;
        this.schedule = schedule;
    }

    public Human(String name, String surname, int year, int iq, Map<DayOfWeek, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }

    public void greetPet() {
        if (getFamily().getPets() != null) {
            for (Pet pet : getFamily().getPets()) {
                System.out.println("Привіт, " + pet.getNickname());
            }
        } else {
            System.out.println("no pets");
        }

    }

    public void describePet() {


        if (getFamily().getPets() != null) {
            for (Pet pet : getFamily().getPets()) {
                if (pet.getTrickLevel() <= 50) {
                    System.out.println("У мене є " + pet.getNickname() + ", їй "
                            + pet.getAge() + " років, він майже не хитрий");
                } else {
                    System.out.println("У мене є " + pet.getNickname() + ", їй "
                            + pet.getAge() + " років, він дуже хитрий");
                }
            }
        } else {
            System.out.println("no pets");
        }



    }

    public void feedPet(boolean itsTimeToFeed) {
        if (getFamily().getPets() != null) {
            for (Pet pet : getFamily().getPets()) {
                Random random = new Random();
                int comparingTrickery = random.nextInt(101);
                if (itsTimeToFeed) {
                    System.out.println("Хм... годувати " + pet.getNickname() + ".");
                } else if (pet.getTrickLevel() > comparingTrickery) {
                    System.out.println("Хм... годувати " + pet.getNickname() + ".");
                } else {
                    System.out.println("Думаю, " + pet.getNickname() + " не голодний.");
                }
            }
        } else {
            System.out.println("no pets");
        }

    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Map<DayOfWeek, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<DayOfWeek, String> schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year && iq == human.iq && Objects.equals(name, human.name) && Objects.equals(surname, human.surname) && Objects.equals(family, human.family) && Objects.equals(schedule, human.schedule);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year, iq, family, schedule);
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + schedule +
                '}';
    }
}
