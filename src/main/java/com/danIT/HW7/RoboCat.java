package com.danIT.HW7;

import java.util.Set;

public class RoboCat extends Pet {


    public RoboCat() {
    }

    public RoboCat(String nickname, int age) {
        super(nickname, age);
    }

    public RoboCat(Species species, String nickname, int age, int trickLevel, Set<String> habits) {
        super(species, nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("Привіт, хазяїн. Я - " + this.getNickname() + ". Я скучив!");
    }
}
