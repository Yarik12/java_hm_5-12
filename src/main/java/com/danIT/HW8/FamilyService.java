package com.danIT.HW8;


import java.util.*;

public class FamilyService {
    private CollectionFamilyDao dataBase = new CollectionFamilyDao();

    public List<Family> getAllFamilies() {
        return dataBase.getAllFamilies();
    }

    public void displayAllFamilies() {
        List<Family> families = dataBase.getAllFamilies();
        int counter = 0;
        for (Family family : families) {
            counter++;
            System.out.println("Family" + counter + ":" + family);
        }
    }

    public List<Family> getFamiliesBiggerThan(int count) {
        List<Family> familiesBiggerThan = new ArrayList<>();
        for (Family allFamily : getAllFamilies()) {
            if (allFamily.countFamily() > count) {
                familiesBiggerThan.add(allFamily);
            }
        }
        return familiesBiggerThan;
    }

    public List<Family> getFamiliesLessThan(int count) {
        List<Family> familiesLessThan = new ArrayList<>();
        for (Family allFamily : getAllFamilies()) {
            if (allFamily.countFamily() < count) {
                familiesLessThan.add(allFamily);
            }
        }
        return familiesLessThan;
    }

    public int countFamiliesWithMemberNumber(int count) {
        List<Family> familiesWithMemberNumber = new ArrayList<>();
        for (Family allFamily : getAllFamilies()) {
            if (allFamily.countFamily() == count) {
                familiesWithMemberNumber.add(allFamily);
            }
        }
        return familiesWithMemberNumber.size();
    }

    public void createNewFamily(Human mother, Human father) {
        Family family = new Family(mother, father);
        dataBase.saveFamily(family);
    }

    public void deleteFamilyByIndex(int index) {
        dataBase.deleteFamily(index);
    }

    public void bornChild(Family family, NamesMan manName, NamesWoman womanName, int currentYear) {
        Random random = new Random();
        int gender = random.nextInt(2);
        int iqCount = (family.getMother().getIq() + family.getFather().getIq()) / 2;
        if (gender == 1) {
            Man man = new Man();
            man.setFamily(family);
            man.setSurname(family.getFather().getSurname());
            man.setName(String.valueOf(manName));
            man.setIq(iqCount);
            man.setYear(currentYear);
            family.addChild(man);
            dataBase.saveFamily(family);
        } else {
            Woman woman = new Woman();
            woman.setFamily(family);
            woman.setName(String.valueOf(womanName));
            woman.setSurname(family.getFather().getSurname());
            woman.setIq(iqCount);
            woman.setYear(currentYear);
            family.addChild(woman);
            dataBase.saveFamily(family);
        }
    }

    public Family adoptChild(Family family, Human child) {
        family.addChild(child);
        dataBase.saveFamily(family);
        return family;
    }

    public void deleteAllChildrenOlderThen(int age, int currentYear) {
        for (Family allFamily : dataBase.getAllFamilies()) {
            Iterator<Human> iterator = allFamily.getChildren().iterator();
            while (iterator.hasNext()) {
                Human child = iterator.next();
                if (currentYear - child.getYear() > age) {
                    iterator.remove();
                    allFamily.deleteChild(child);
                }
            }
        }
    }


    public int count() {
        return dataBase.getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return dataBase.getFamilyByIndex(index);
    }

    public Set<Pet> getPets(int familyIndex) {
        return getFamilyById(familyIndex).getPets();
    }

    public void addPet(int familyIndex, Pet pet) {
        if(getFamilyById(familyIndex).getPets() != null){
            getFamilyById(familyIndex).getPets().add(pet);
            dataBase.saveFamily(getFamilyById(familyIndex));
        }else {
            Set<Pet> newPets = new HashSet<>();
            getFamilyById(familyIndex).setPets(newPets);
            getFamilyById(familyIndex).getPets().add(pet);
            dataBase.saveFamily(getFamilyById(familyIndex));
        }

    }
}
