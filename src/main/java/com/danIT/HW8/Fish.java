package com.danIT.HW8;

import java.util.Set;

public class Fish extends Pet {


    public Fish() {
    }

    public Fish(Species species, String nickname, int age, int trickLevel, Set<String> habits) {
        super(species, nickname, age, trickLevel, habits);
    }

    public Fish(String nickname, int age) {
        super(nickname, age);
    }

    @Override
    public void respond() {
        System.out.println("Привіт, хазяїн. Я - " + this.getNickname() + ". Я скучив!");
    }
}
