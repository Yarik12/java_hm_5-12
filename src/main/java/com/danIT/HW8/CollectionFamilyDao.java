package com.danIT.HW8;

import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {

    private List<Family> familyDataBase = new ArrayList<>();



    @Override
    public List<Family> getAllFamilies() {
        return familyDataBase;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        if (index >= 0 && index < familyDataBase.size()){
            return familyDataBase.get(index);
        }
        return null;
    }

    @Override
    public boolean deleteFamily(int index) {
        if (index >= 0 && index < familyDataBase.size()){
            familyDataBase.remove(index);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteFamily(Family family) {
        return familyDataBase.remove(family);
    }

    @Override
    public boolean saveFamily(Family family) {
        int familyIndex = familyDataBase.indexOf(family);
        if(familyIndex != -1){
            familyDataBase.set(familyIndex,family);
        }else {
            familyDataBase.add(family);
        }
        return true;
    }
}
