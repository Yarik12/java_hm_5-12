package com.danIT.HW11;

import java.text.ParseException;
import java.util.Scanner;

public class ConsoleApp {
    FamilyService familyService = new FamilyService();
    FamilyController familyController = new FamilyController(familyService);
    private static boolean exit = true;

    private static Scanner scanner = new Scanner(System.in);

    public void console() throws ParseException {
        while (exit) {
            menu();
            System.out.println("Оберіть команду:");
            int select = 0;
            select = scanner.nextInt();

            switch (select) {
                case 1 -> testData();
                case 2 -> familyController.displayAllFamilies();
                case 3 -> getFamiliesBiggerThan();
                case 4 -> getFamiliesLessThan();
                case 5 -> countFamiliesWithMemberNumber();
                case 6 -> {
                    try {
                        createNewFamily();
                    } catch (ParseException e) {
                        throw new RuntimeException(e);
                    }
                }
                case 7 -> deleteFamilyByIndex();
                case 8 -> {
                    try {
                        editFamilyByIndex();
                    } catch (ParseException e) {
                        throw new RuntimeException(e);
                    }

                }
                case 9 -> deleteAllChildrenOlderThen();
                case 0 -> exit = false;
                default -> System.out.println("Неправильна цифра");
            }
        }
    }

    public void menu() {

        System.out.println("1. Заповнити тестовими даними (автоматом створити кілька сімей та зберегти їх у базі)\n" +
                "2. Відобразити весь список сімей (відображає список усіх сімей з індексацією, що починається з 1)\n" +
                "3. Відобразити список сімей, де кількість людей більша за задану\n" +
                "4. Відобразити список сімей, де кількість людей менша за задану\n" +
                "5. Підрахувати кількість сімей, де кількість членів дорівнює\n" +
                "6. Створити нову родину\n" +
                "7. Видалити сім'ю за індексом сім'ї у загальному списку\n" +
                "8. Редагувати сім'ю за індексом сім'ї у загальному списку\n" +
                "   - 1. Народити дитину\n" +
                "   - 2. Усиновити дитину\n" +
                "   - 3. Повернутися до головного меню\n" +
                "- 9. Видалити всіх дітей старше віку\n" +
                "- 0. Вийти з консолі"
        );

    }

    public void testData() {
        Human mother = new Human("Ann", "Melnik", 1990);
        Human father = new Human("Oleh", "Melnik", 1988);
        Human father1 = new Human("Ivan", "Metelitsa", 1980);
        Human mother1 = new Human("Ira", "Ivanova", 1990);
        Human child1 = new Human("Ivan", "Melnik", 2010);
        Human child2 = new Human("Sofi", "Melnik", 2018);
        Human child3 = new Human("Anton", "Ivanov", 2003);
        familyController.createNewFamily(mother1, father1);
        familyController.createNewFamily(mother, father);
        familyController.getFamilyById(1).addChild(child1);
        familyController.getFamilyById(1).addChild(child2);
        familyController.getFamilyById(1).addChild(child3);
        familyController.bornChild(familyController.getFamilyById(0), "DANIEL", "BARBARA", 2024);
        familyController.adoptChild(familyController.getFamilyById(0),
                new Human("Denis", "Melnik", 2015));

    }

    public void getFamiliesBiggerThan() {
        int count = 0;
        System.out.println("Введіть кількість членів родини: ");
        count = scanner.nextInt();
        if (count <= 0) {
            System.out.println("Кількість членів родини не може дорівнювати нулю або бути від'ємною");
        } else {
            System.out.println(familyController.getFamiliesBiggerThan(count));
        }

    }

    public void getFamiliesLessThan() {
        int count = 0;
        System.out.println("Введіть кількість членів родини: ");
        count = scanner.nextInt();
        if (count <= 0) {
            System.out.println("Кількість членів родини не може дорівнювати нулю або бути від'ємною");
        } else {
            System.out.println(familyController.getFamiliesLessThan(count));
        }

    }

    public void countFamiliesWithMemberNumber() {
        int count = 0;
        System.out.println("Введіть кількість членів родини: ");
        count = scanner.nextInt();
        if (count <= 0) {
            System.out.println("Кількість членів родини не може дорівнювати нулю або бути від'ємною");
        } else {
            System.out.println(familyController.countFamiliesWithMemberNumber(count));
        }

    }

    public void createNewFamily() throws ParseException {
        String a = scanner.nextLine();
        boolean check = true;
        System.out.println("Введіть ім'я матері:");
        String motherName = scanner.nextLine();

        System.out.println("Введіть фамілію матері:");
        String motherSurname = scanner.nextLine();

        System.out.println("Введіть дату народження матері у форматі 10/10/2020:");
        String motherBirthDate = scanner.nextLine();

        int motherIq = 0;
        while (check) {
            System.out.println("Введіть iq матері:");
            motherIq = scanner.nextInt();

            if (motherIq <= 0) {
                System.out.println("IQ не може дорівнювати нулю або бути від'ємною");
            } else {
                check = false;
            }
        }

        check = true;

        Human mother = new Human(motherName, motherSurname, motherBirthDate, motherIq);

        String b = scanner.nextLine();

        System.out.println("Введіть ім'я батька:");
        String fatherName = scanner.nextLine();


        System.out.println("Введіть фамілію батька:");
        String fatherSurname = scanner.nextLine();


        System.out.println("Введіть дату народження батька у форматі 10/10/2020:");
        String fatherBirthDate = scanner.nextLine();


        int fatherIq = 0;
        while (check) {

            System.out.println("Введіть iq батька:");
            fatherIq = scanner.nextInt();

            if (fatherIq <= 0) {
                System.out.println("IQ не може дорівнювати нулю або бути від'ємною");
            } else {
                check = false;
            }
        }

        Human father = new Human(fatherName, fatherSurname, fatherBirthDate, fatherIq);

        System.out.println("Створення нової сім'ї");

        familyController.createNewFamily(mother, father);

    }

    public void deleteFamilyByIndex() {
        System.out.println("Введіть індекс сім'ї яку хочете видалити");
        int index = 0;
        index = scanner.nextInt();
        if (index <= 0) {
            System.out.println("Індекс не може дорівнювати нулю або бути від'ємним");
        } else {
            familyController.deleteFamilyByIndex(index);
        }

    }

    public void editFamilyByIndex() throws ParseException {

        boolean exit = true;
        while (exit) {
            System.out.println("Оберіть дію:\n" +
                    "   - 1. Народити дитину\n" +
                    "   - 2. Усиновити дитину\n" +
                    "   - 3. Повернутися до головного меню\n");
            int select = 0;
            select = scanner.nextInt();
            switch (select) {
                case 1 -> bornChild();
                case 2 -> adoptChild();
                case 3 -> exit = false;
                default -> System.out.println("Неправильна цифра");
            }
        }
    }

    public void bornChild() {
        System.out.println("Введіть індекс сім'ї: ");
        int index = 0;
        index = scanner.nextInt();

        if (index <= 0) {
            System.out.println("Індекс не може дорівнювати нулю або бути від'ємним");
        } else {
            String a = scanner.nextLine();
            System.out.println("Введіть ім'я для хлопчика ");
            String boyName = "";
            boyName = scanner.nextLine();
            System.out.println("Введіть ім'я для дівчинці ");
            String girlName = "";
            girlName = scanner.nextLine();
            System.out.println("Введіть поточний рік ");
            int currentYear = 0;
            currentYear = scanner.nextInt();

            Family family = familyController.getFamilyById(index-1);

            familyController.bornChild(family, boyName, girlName, currentYear);
        }
    }

    public void adoptChild() throws ParseException {
        System.out.println("Введіть індекс сім'ї: ");
        int index = 0;
        index = scanner.nextInt();

        if (index <= 0) {
            System.out.println("Індекс не може дорівнювати нулю або бути від'ємним");
        } else {
            System.out.println("Введіть ім'я дитини: ");
            String name = "";
            name = scanner.nextLine();
            System.out.println("Введіть фамілію дитини: ");
            String surname = "";
            surname = scanner.nextLine();
            System.out.println("Введіть рік народження дитини: ");
            String birthDate = "";
            birthDate = scanner.nextLine();
            System.out.println("Введіть iq дитини: ");
            int iq = 0;
            iq = scanner.nextInt();

            Family family = familyController.getFamilyById(index-1);

            Human child = new Human(name, surname, birthDate, iq);

            familyController.adoptChild(family, child);
        }



    }

    public void deleteAllChildrenOlderThen() {
        System.out.println("Введіть вік: ");
        int age = 0;
        if(age <= 0){
            System.out.println("Вік не може дорівнювати нулю або бути від'ємним");
        }else {
            age = scanner.nextInt();
            System.out.println("Введіть поточний рік: ");
            int currentYear = 0;
            currentYear = scanner.nextInt();
            familyController.deleteAllChildrenOlderThen(age, currentYear);
        }
    }
}


