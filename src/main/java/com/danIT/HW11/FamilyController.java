package com.danIT.HW11;

import java.util.List;
import java.util.Set;

public class FamilyController {
    private FamilyService familyService;
    private static final int MAX_FAMILY_SIZE = 5;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }


    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int count) {
        return familyService.getFamiliesBiggerThan(count);
    }

    public List<Family> getFamiliesLessThan(int count) {
        return familyService.getFamiliesLessThan(count);
    }

    public int countFamiliesWithMemberNumber(int count) {
        return  familyService.countFamiliesWithMemberNumber(count);
    }

    public void createNewFamily(Human mother, Human father) {
        familyService.createNewFamily(mother, father);
    }

    public void deleteFamilyByIndex(int index) {
        familyService.deleteFamilyByIndex(index);
    }

    public void bornChild(Family family, String manName, String womanName, int currentYear) {
        if(family.countFamily()>= MAX_FAMILY_SIZE){
            throw new FamilyOverflowException("Сім'я перевищила максимально допустимий розмір");
        }else {
            familyService.bornChild(family,manName,womanName,currentYear);
        }

    }

    public Family adoptChild(Family family, Human child) {
        if(family.countFamily()>= MAX_FAMILY_SIZE){
            throw new FamilyOverflowException("Сім'я перевищила максимально допустимий розмір");
        }else {
            return familyService.adoptChild(family, child);
        }
    }

    public void deleteAllChildrenOlderThen(int age, int currentYear) {
        familyService.deleteAllChildrenOlderThen(age, currentYear);
    }

    public int count() {
        return familyService.count();
    }

    public Family getFamilyById(int index) {
        return familyService.getFamilyById(index);
    }

    public Set<Pet> getPets(int familyIndex) {
        return familyService.getPets(familyIndex);
    }

    public void addPet(int familyIndex, Pet pet) {
        familyService.addPet(familyIndex, pet);
    }

}
