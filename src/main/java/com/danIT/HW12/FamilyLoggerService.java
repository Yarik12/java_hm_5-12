package com.danIT.HW12;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class FamilyLoggerService {

    public static void info(String message) {
        log("[INFO]", message);
    }

    public static void error(String message) {
        log("[ERROR]", message);
    }

    private static void log(String level, String message) {
        String logFileName = "application.log";
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(logFileName, true))) {
            LocalDateTime now = LocalDateTime.now();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm");
            String formattedDateTime = now.format(formatter);
            String logMessage = formattedDateTime + " " + level + " " + message + System.lineSeparator();
            writer.write(logMessage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
