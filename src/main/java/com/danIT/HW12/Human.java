package com.danIT.HW12;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Human implements Serializable {
    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Family family;
    private Map<DayOfWeek, String> schedule;


    public Human() {
    }

    public Human(String name, String surname, long birthDate) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
    }

    public Human(String name, String surname, long birthDate, int iq, Map<DayOfWeek, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq = iq;
        this.schedule = schedule;
    }

    public Human(String name, String surname, long birthDate, int iq, Family family, Map<DayOfWeek, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq = iq;
        this.family = family;
        this.schedule = schedule;
    }

    public Human(String name, String surname, String birthDateStr, int iq) throws ParseException {
        this.name = name;
        this.surname = surname;
        this.iq = iq;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date date = sdf.parse(birthDateStr);
            this.birthDate = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    public String prettyFormat() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return "name= " + name + ", surname= " + surname + ", birthDate= " +
                simpleDateFormat.format(new Date(birthDate)) + ", iq= " + iq + ", schedule= " + schedule +"\n";


    }

    public void greetPet() {
        if (getFamily().getPets() != null) {
            for (Pet pet : getFamily().getPets()) {
                System.out.println("Привіт, " + pet.getNickname());
            }
        } else {
            System.out.println("no pets");
        }

    }

    public void describePet() {


        if (getFamily().getPets() != null) {
            for (Pet pet : getFamily().getPets()) {
                if (pet.getTrickLevel() <= 50) {
                    System.out.println("У мене є " + pet.getNickname() + ", їй "
                            + pet.getAge() + " років, він майже не хитрий");
                } else {
                    System.out.println("У мене є " + pet.getNickname() + ", їй "
                            + pet.getAge() + " років, він дуже хитрий");
                }
            }
        } else {
            System.out.println("no pets");
        }


    }

    public void feedPet(boolean itsTimeToFeed) {
        if (getFamily().getPets() != null) {
            for (Pet pet : getFamily().getPets()) {
                Random random = new Random();
                int comparingTrickery = random.nextInt(101);
                if (itsTimeToFeed) {
                    System.out.println("Хм... годувати " + pet.getNickname() + ".");
                } else if (pet.getTrickLevel() > comparingTrickery) {
                    System.out.println("Хм... годувати " + pet.getNickname() + ".");
                } else {
                    System.out.println("Думаю, " + pet.getNickname() + " не голодний.");
                }
            }
        } else {
            System.out.println("no pets");
        }

    }

    public String describeAge() {
        long currentTime = System.currentTimeMillis();
        long ageMiles = currentTime - birthDate;
        long days = TimeUnit.MILLISECONDS.toDays(ageMiles);
        long years = days / 365;
        long months = (days % 365) / 30;
        days -= (years * 365 + months * 30);

        return String.format("years: %d\nmonths: %d\ndays: %d ", years, months, days);

    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Map<DayOfWeek, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<DayOfWeek, String> schedule) {
        this.schedule = schedule;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return birthDate == human.birthDate && iq == human.iq && Objects.equals(name, human.name) && Objects.equals(surname, human.surname) && Objects.equals(family, human.family) && Objects.equals(schedule, human.schedule);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDate, iq, family, schedule);
    }

    @Override
    public String toString() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birthDate=" + simpleDateFormat.format(new Date(birthDate)) +
                ", iq=" + iq +
                ", schedule=" + schedule +
                '}';
    }
}
