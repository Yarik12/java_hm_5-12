package com.danIT.HW12;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class CollectionFamilyDao implements FamilyDao {

    private List<Family> familyDataBase = new ArrayList<>();


    @Override
    public List<Family> getAllFamilies() {
        FamilyLoggerService.info("отримання списку сімей");
        return familyDataBase;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        if (index >= 0 && index < familyDataBase.size()) {
            FamilyLoggerService.info("отримання сім'ї за індексом");
            return familyDataBase.get(index);
        }else {
            FamilyLoggerService.error("помилка при отриманні сім'ї за індексом");
            return null;
        }

    }

    @Override
    public boolean deleteFamily(int index) {
        if (index >= 0 && index < familyDataBase.size()) {
            familyDataBase.remove(index);
            FamilyLoggerService.info("видалення сім'ї за індексом");
            return true;
        }else {
            FamilyLoggerService.error("помилка при видаленні сім'ї за індексом");
            return false;
        }

    }

    @Override
    public boolean deleteFamily(Family family) {
        FamilyLoggerService.info("видалення сім'ї");
        return familyDataBase.remove(family);
    }

    @Override
    public boolean saveFamily(Family family) {
        int familyIndex = familyDataBase.indexOf(family);
        if (familyIndex != -1) {
            familyDataBase.set(familyIndex, family);
        } else {
            familyDataBase.add(family);
        }
        FamilyLoggerService.info("збереження сім'ї у базу даних");
        return true;
    }

    @Override
    public void loadData(List<Family> families) {
        FamilyLoggerService.info("завантаження сім'ї у базу даних");
        familyDataBase = families;
    }

    public void loadingTestDataFromAFile() {
        List<Family> families;
        try (FileInputStream fileIn = new FileInputStream("testDataFile.ser");
             ObjectInputStream objectIn = new ObjectInputStream(fileIn)) {
            families = (List<Family>) objectIn.readObject();
            FamilyLoggerService.info("завантаження тестових сімей у базу даних з файлу");
        } catch (IOException | ClassNotFoundException e) {
            FamilyLoggerService.error("помилка при завантаженні тестових сімей у базу даних з файлу");
            throw new RuntimeException(e);
        }
        loadData(families);
    }

    public void loadingDataFromAFile() {
        List<Family> families;
        try (FileInputStream fileIn = new FileInputStream("dataFile.ser");
             ObjectInputStream objectIn = new ObjectInputStream(fileIn)) {
            families = (List<Family>) objectIn.readObject();
            FamilyLoggerService.info("завантаження сімей у базу даних з файлу");
        } catch (IOException | ClassNotFoundException e) {
            FamilyLoggerService.error("помилка при завантаженні сімей у базу даних з файлу");
            throw new RuntimeException(e);
        }
        loadData(families);

    }

    public void writingDataToAFile(List<Family> families) {
        try (FileOutputStream fileOut = new FileOutputStream("dataFile.ser");
             ObjectOutputStream objectOut = new ObjectOutputStream(fileOut)) {
            objectOut.writeObject(families);
            FamilyLoggerService.info("запис сімей у файл з бази даних");
        } catch (IOException e) {
            FamilyLoggerService.error("помилка при записі у файл сімей з базі даних");
            throw new RuntimeException(e);
        }
    }


}
