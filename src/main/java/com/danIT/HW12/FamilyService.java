package com.danIT.HW12;


import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

public class FamilyService {
    private CollectionFamilyDao dataBase = new CollectionFamilyDao();

    public List<Family> getAllFamilies() {
        return dataBase.getAllFamilies();
    }

    public void loadingTestDataFromAFile(){
        dataBase.loadingTestDataFromAFile();
    }

    public void loadingDataFromAFile(){
        dataBase.loadingDataFromAFile();
    }

    public void writingDataToAFile(List<Family> families){
        dataBase.writingDataToAFile(families);
    }

    public void displayAllFamilies() {

        List<Family> families = dataBase.getAllFamilies();
        if (families != null) {
            families.stream()
                    .forEachOrdered(family -> System.out.println(families.indexOf(family) + 1 + " " + family.prettyFormat()));
        } else {
            System.out.println("Список сімей пустий");
        }

//        int counter = 0;
//        for (Family family : families) {
//            counter++;
//            System.out.println("Family" + counter + ":" + family);
//        }
    }

    public List<Family> getFamiliesBiggerThan(int count) {
        return getAllFamilies().stream()
                .filter(family -> family.countFamily() > count)
                .collect(Collectors.toList());

//        List<Family> familiesBiggerThan = new ArrayList<>();
//        for (Family allFamily : getAllFamilies()) {
//            if (allFamily.countFamily() > count) {
//                familiesBiggerThan.add(allFamily);
//            }
//        }
//        return familiesBiggerThan;
    }

    public List<Family> getFamiliesLessThan(int count) {
        return getAllFamilies().stream()
                .filter(family -> family.countFamily() < count)
                .collect(Collectors.toList());

//        List<Family> familiesLessThan = new ArrayList<>();
//        for (Family allFamily : getAllFamilies()) {
//            if (allFamily.countFamily() < count) {
//                familiesLessThan.add(allFamily);
//            }
//        }
//        return familiesLessThan;
    }

    public int countFamiliesWithMemberNumber(int count) {
        return (int) getAllFamilies().stream()
                .filter(family -> family.countFamily() == count)
                .count();

//        List<Family> familiesWithMemberNumber = new ArrayList<>();
//        for (Family allFamily : getAllFamilies()) {
//            if (allFamily.countFamily() == count) {
//                familiesWithMemberNumber.add(allFamily);
//            }
//        }
//        return familiesWithMemberNumber.size();
    }

    public void createNewFamily(Human mother, Human father) {
        Family family = new Family(mother, father);
        dataBase.saveFamily(family);
    }

    public void deleteFamilyByIndex(int index) {
        dataBase.deleteFamily(index);
    }

    public void bornChild(Family family, String manName, String womanName, int currentYear) {
        Random random = new Random();
        int gender = random.nextInt(2);
        int iqCount = (family.getMother().getIq() + family.getFather().getIq()) / 2;
        if (gender == 1) {
            Man man = new Man();
            man.setFamily(family);
            man.setSurname(family.getFather().getSurname());
            man.setName(String.valueOf(manName));
            man.setIq(iqCount);
            man.setBirthDate(currentYear);
            family.addChild(man);
            dataBase.saveFamily(family);
        } else {
            Woman woman = new Woman();
            woman.setFamily(family);
            woman.setName(String.valueOf(womanName));
            woman.setSurname(family.getFather().getSurname());
            woman.setIq(iqCount);
            woman.setBirthDate(currentYear);
            family.addChild(woman);
            dataBase.saveFamily(family);
        }
    }

    public Family adoptChild(Family family, Human child) {
        family.addChild(child);
        dataBase.saveFamily(family);
        return family;
    }

    public void deleteAllChildrenOlderThen(int age, int currentYear) {
        getAllFamilies().forEach(family -> {
            List<Human> childrenOlderThen = family.getChildren().stream()
                    .filter(child -> (currentYear - child.getBirthDate()) > age)
                    .collect(Collectors.toList());
            for (Human human : childrenOlderThen) {
                family.deleteChild(human);
            }
            dataBase.saveFamily(family);
        });

//        for (Family allFamily : dataBase.getAllFamilies()) {
//            Iterator<Human> iterator = allFamily.getChildren().iterator();
//            while (iterator.hasNext()) {
//                Human child = iterator.next();
//                if (currentYear - child.getBirthDate() > age) {
//                    iterator.remove();
//                    allFamily.deleteChild(child);
//                }
//            }
//        }
    }


    public int count() {
        return dataBase.getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return dataBase.getFamilyByIndex(index);
    }

    public Set<Pet> getPets(int familyIndex) {
        return getFamilyById(familyIndex).getPets();
    }

    public void addPet(int familyIndex, Pet pet) {
        if (getFamilyById(familyIndex).getPets() != null) {
            getFamilyById(familyIndex).getPets().add(pet);
            dataBase.saveFamily(getFamilyById(familyIndex));
        } else {
            Set<Pet> newPets = new HashSet<>();
            getFamilyById(familyIndex).setPets(newPets);
            getFamilyById(familyIndex).getPets().add(pet);
            dataBase.saveFamily(getFamilyById(familyIndex));
        }

    }

    public void loadData(List<Family> families) {
        dataBase.loadData(families);
    }
}
