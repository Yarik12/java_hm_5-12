package com.danIT.HW12;

import java.text.ParseException;
import java.util.*;

public class Main {
    public static void main(String[] args) throws ParseException {
        Set<String> habits = new HashSet<>();
        habits.add("loves to play with the ball");
        habits.add("loves to chase a stick");

        Map<DayOfWeek, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.MONDAY, "Ldo home work");
        schedule.put(DayOfWeek.TUESDAY, "go to courses; watch a film;");
        schedule.put(DayOfWeek.WEDNESDAY, "pass the English exam;");
        schedule.put(DayOfWeek.THURSDAY, "go to the doctor;");
        schedule.put(DayOfWeek.FRIDAY, "pay for utilities;");
        schedule.put(DayOfWeek.SATURDAY, "write a resume;");
        schedule.put(DayOfWeek.SUNDAY, "go have a beer with friends;");


        Human mother = new Human("Ann", "Melnik", 1990, 100, schedule);
        Human father = new Human("Oleh", "Melnik", 1988, 100, schedule);
        Human child1 = new Human("Ivan", "Melnik", 2010, 100, schedule);
        Human child2 = new Human("Sofi", "Melnik", 2018, 100, schedule);
        Human child3 = new Human("Anton", "Ivanov", 2003, 100, schedule);

        Dog dog = new Dog(Species.DOG, "Barsik", 6, 60, habits);
        Fish fish = new Fish(Species.FISH, "Dori", 1, 10, habits);

        Set<Pet> pets = new HashSet<>();
        pets.add(dog);
        pets.add(fish);

        List<Human> children = new ArrayList<>();
        children.add(child1);
        children.add(child2);
        children.add(child3);


        Family family = new Family(mother, father, children, pets);
        family.addChild(child1);
        family.addChild(child2);
        family.addChild(child3);
        System.out.println(family.getChildren().toString());
        family.deleteChild(child3);
        System.out.println(family.getChildren().toString());

        mother.setFamily(family);
        father.setFamily(family);

        mother.feedPet(true);

        father.feedPet(false);

        mother.greetPet();

        father.describePet();

        family.countFamily();

        family.bornChild();

        System.out.println(family.getChildren().toString());

        System.out.println(dog.toString());

        System.out.println("----------------HW8-Main----------------");


        FamilyService familyService = new FamilyService();
        FamilyController familyController = new FamilyController(familyService);

        Human father1 = new Human("Ivan", "Metelitsa", 1980);
        Human mother1 = new Human("Ira", "Ivanova", 1990);
        familyController.createNewFamily(mother1, father1);
        familyController.createNewFamily(mother, father);
        familyController.getFamilyById(1).addChild(child1);
        familyController.getFamilyById(1).addChild(child2);
        familyController.getFamilyById(1).addChild(child3);
        familyController.displayAllFamilies();
        System.out.println("--------------getFamiliesMethods------------------------");
        System.out.println(familyController.getFamiliesBiggerThan(2));
        System.out.println(familyController.getFamiliesLessThan(3));
        System.out.println(familyController.countFamiliesWithMemberNumber(2));
        System.out.println("--------------newChildMethods---------------------------");
        familyController.bornChild(familyController.getFamilyById(0), "DANIEL", "BARBARA", 2024);
        familyController.adoptChild(familyController.getFamilyById(0),
                new Human("Denis", "Melnik", 2015));
        familyController.displayAllFamilies();
        System.out.println("--------------deleteAllChildrenOlderThen----------------");
        familyController.deleteAllChildrenOlderThen(13, 2024);
        familyController.displayAllFamilies();

        familyController.addPet(0, new Dog(Species.DOG, "Barsik", 6, 60, habits));
        System.out.println(familyController.getFamilyById(0).getPets());

        Human adoptedChild = new Human("Igor", "Melnik", "01/12/2020", 70);
        System.out.println(adoptedChild.describeAge());
        System.out.println(adoptedChild.toString());
        System.out.println("----------------prettyFormatCheck--------------");


        System.out.println( familyController.getFamilyById(0).prettyFormat());

        System.out.println("----------------ConsoleApp--------------");
        ConsoleApp consoleApp = new ConsoleApp();
        consoleApp.console();
    }
}
