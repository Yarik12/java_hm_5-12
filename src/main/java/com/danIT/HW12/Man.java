package com.danIT.HW12;


import java.io.Serializable;
import java.text.ParseException;
import java.util.Map;

public final class Man extends Human implements Serializable {

    public Man() {
    }

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq, Family family, Map<DayOfWeek, String> schedule) {
        super(name, surname, year, iq, family, schedule);
    }

    public Man(String name, String surname, int year, int iq, Map<DayOfWeek, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Man(String name, String surname, String birthDateStr, int iq) throws ParseException {
        super(name, surname, birthDateStr, iq);
    }

    public void repairCar() {
        System.out.println("Лагоджу авто");
    }

    public void greetPet() {
        if (getFamily().getPets() != null) {
            for (Pet pet : getFamily().getPets()) {
                System.out.println("Привіт, " + pet.getNickname());
            }
        } else {
            System.out.println("no pets");
        }

    }
}
