package com.danIT.HW9;

import java.util.*;

public class Family implements HumanCreator {
    private Human mother;
    private Human father;
    private List<Human> children;
    private Set<Pet> pets;

    public Family(Human mother, Human father, List<Human> children, Set<Pet> pets) {
        if ((mother != null && father != null)) {
            this.mother = mother;
            this.father = father;
            this.children = children;
            this.pets = pets;
        }
    }

    //if ((mother != null && father != null)) {

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
    }

    public Family(Human mother, Human father, List<Human> children) {
        this.mother = mother;
        this.father = father;
        this.children = children;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public Set<Pet> getPets() {
        return pets;
    }

    public void setPets(Set<Pet> pets) {
        this.pets = pets;
    }

    public void addChild(Human child) {
        if(children != null){
            children.add(child);
            child.setFamily(this);
        }else {
            List<Human> childrenNew = new ArrayList<>();
            childrenNew.add(child);
            children = childrenNew;
        }
    }

    public boolean deleteChild(Human child) {
        if (children.isEmpty()) {
            System.out.println("В цій сім'ї немає дітей");
            return false;
        } else {
            children.remove(child);
            child.setFamily(null);
            return true;
        }

    }


    public int countFamily() {
        if(children == null){
            return 2;
        }
        int familyCount = children.size() + 2;
        return familyCount;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + children +
                ", pets=" + pets +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) && Objects.equals(father, family.father) && Objects.equals(children, family.children) && Objects.equals(pets, family.pets);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father, children, pets);
    }

    @Override
    public void bornChild() {
        Random random = new Random();
        int gender = random.nextInt(2);
        int indexOfName = random.nextInt(NamesMan.values().length);
        int iqCount = (mother.getIq() + father.getIq()) / 2;
        if (gender == 1) {
            Man man = new Man();
            man.setFamily(this);
            man.setSurname(this.father.getSurname());
            man.setName(String.valueOf(NamesMan.values()[indexOfName]));
            man.setIq(iqCount);
            this.addChild(man);
        } else {
            Woman woman = new Woman();
            woman.setFamily(this);
            woman.setName(String.valueOf(NamesWoman.values()[indexOfName]));
            woman.setSurname(this.father.getSurname());
            woman.setIq(iqCount);
            this.addChild(woman);
        }
    }


}
