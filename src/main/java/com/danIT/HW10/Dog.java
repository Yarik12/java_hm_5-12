package com.danIT.HW10;

import java.util.Set;

public class Dog extends Pet implements Foul {


    public Dog() {
    }

    public Dog(Species species, String nickname, int age, int trickLevel, Set<String> habits) {
        super(species, nickname, age, trickLevel, habits);
    }

    public Dog(String nickname, int age) {
        super(nickname, age);
    }

    @Override
    public void foul() {
        System.out.println("Потрібно добре замести сліди...");
    }

    @Override
    public void respond() {
        System.out.println("Привіт, хазяїн. Я - " + this.getNickname() + ". Я скучив!");
    }
}
