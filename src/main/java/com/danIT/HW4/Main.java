package com.danIT.HW4;

import com.danIT.HW5.Species;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Human mother = new Human("Ann", "Melnik", 1990);
        Human father = new Human("Oleh", "Melnik", 1988);
        Human child1 = new Human("Ivan", "Melnik", 2010);
        Human child2 = new Human("Sofi", "Melnik", 2018);
        Human child3 = new Human("Anton", "Ivanov", 2003);
        Pet pet = new Pet(Species.DOG, "Barsik", 6, 60, new String[]{"loves to play with the ball"
                , "loves to chase a stick"});
        Family family = new Family(mother, father, new Human[]{child1, child2}, pet);
        family.addChild(child1);
        family.addChild(child2);
        family.addChild(child3);
        System.out.println(Arrays.toString(family.getChildren()));
        family.deleteChild(child3);
        System.out.println(Arrays.toString(family.getChildren()));
        mother.feedPet(true);

        father.feedPet(false);

        mother.greetPet();

        father.describePet();

        family.countFamily();
        System.out.println(pet.toString());
    }
}
