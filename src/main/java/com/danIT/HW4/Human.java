package com.danIT.HW4;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Family family;


    private String[][] schedule = new String[7][2];

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, int iq, Human mother, Human father, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }

    public Human() {
    }

    public void greetPet() {
        System.out.println("Привіт, " + this.family.getPet().getNickname());
    }

    public void describePet() {
        if (this.family.getPet().getTrickLevel() <= 50) {
            System.out.println("У мене є " + this.family.getPet().getNickname() + ", їй "
                    + this.family.getPet().getAge() + " років, він майже не хитрий");
        } else {
            System.out.println("У мене є " + this.family.getPet().getNickname() + ", їй "
                    + this.family.getPet().getAge() + " років, він дуже хитрий");
        }

    }

    public void feedPet(boolean itsTimeToFeed) {
        Random random = new Random();
        int comparingTrickery = random.nextInt(101);
        if (itsTimeToFeed) {
            System.out.println("Хм... годувати " + family.getPet().getNickname() + ".");
        }else if (family.getPet().getTrickLevel() > comparingTrickery) {
            System.out.println("Хм... годувати " + family.getPet().getNickname() + ".");
        } else {
            System.out.println("Думаю, " + family.getPet().getNickname() + " не голодний.");
        }
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                '}';
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            System.out.println("Видалення об'єкту класу Human " + this);
        } finally {
            super.finalize();
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return iq == human.iq && Objects.equals(family, human.family) && Arrays.equals(schedule, human.schedule);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(iq, family);
        result = 31 * result + Arrays.hashCode(schedule);
        return result;
    }
}
