package com.danIT.HW5;

import java.util.Arrays;
import java.util.Objects;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;

    public Family(Human mother, Human father, Human[] children, Pet pet) {
        if ((mother != null && father != null)) {
            this.mother = mother;
            this.father = father;
            this.mother.setFamily(this);
            this.father.setFamily(this);
            this.children = new Human[0];
            this.pet = pet;
        }
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
    }

    public Family(Human mother, Human father, Human[] children) {
        this.mother = mother;
        this.father = father;
        this.children = children;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public void addChild(Human child) {
        child.setFamily(this);
        Human[] newChildrenArr = Arrays.copyOf(children, children.length + 1);
        newChildrenArr[newChildrenArr.length - 1] = child;
        children = newChildrenArr;
    }

    public boolean deleteChild(Human child) {
        int childIndex = findAChild(child);
        if(childIndex == -1){
            System.out.println("Такої дитини немає в см'ї!");
            return false;
        }
        Human[] newChildren = new Human[children.length-1];
        System.arraycopy(children, 0, newChildren, 0, childIndex);
        System.arraycopy(children, childIndex + 1, newChildren, childIndex, children.length
                - childIndex - 1);
        children = newChildren;
        System.out.println("The child left the family");
        child.setFamily(null);
        return true;
    }

    public int findAChild(Human child) {
        int index = -1;
        for (int i = 0; i < children.length; i++) {
            if (children[i].equals(child)) {
                index = i;
            }
        }
        return index;
    }

    public int countFamily() {
        int familyCount = children.length + 2;
        System.out.println("Кількість осіб у сім'ї: " + familyCount);
        return familyCount;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + Arrays.toString(children) +
                ", pet=" + pet +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) && Objects.equals(father,
                family.father) && Arrays.equals(children, family.children) && Objects.equals(pet, family.pet);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pet);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }
}
