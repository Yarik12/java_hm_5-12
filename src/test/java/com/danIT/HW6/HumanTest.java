package com.danIT.HW6;

import com.danIT.HW5.DayOfWeek;
import com.danIT.HW5.Human;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {

    @Test
    void testEquals() {
        String[][] scedule = {
                {DayOfWeek.SUNDAY.name(), "do home work"},
                {DayOfWeek.MONDAY.name(), "go to courses; watch a film;"},
                {DayOfWeek.TUESDAY.name(), "pass the English exam;"},
                {DayOfWeek.WEDNESDAY.name(), "go to the doctor;"},
                {DayOfWeek.THURSDAY.name(), "pay for utilities;"},
                {DayOfWeek.FRIDAY.name(), "write a resume;"},
                {DayOfWeek.SATURDAY.name(), "go have a beer with friends;"}
        };
        Human human = new Human("Anton", "Ivanov", 2003, 75, scedule);
        Human human1 = new Human("Ann", "Melnik", 1990, 23, scedule);
        Human human2 = new Human("Ann", "Melnik", 1990, 23, scedule);
        assertNotEquals(human,human1);
        assertEquals(human1,human2);
    }

    @Test
    void testHashCode() {
        String[][] scedule = {
                {DayOfWeek.SUNDAY.name(), "do home work"},
                {DayOfWeek.MONDAY.name(), "go to courses; watch a film;"},
                {DayOfWeek.TUESDAY.name(), "pass the English exam;"},
                {DayOfWeek.WEDNESDAY.name(), "go to the doctor;"},
                {DayOfWeek.THURSDAY.name(), "pay for utilities;"},
                {DayOfWeek.FRIDAY.name(), "write a resume;"},
                {DayOfWeek.SATURDAY.name(), "go have a beer with friends;"}
        };
        Human human = new Human("Anton", "Ivanov", 2003, 75, scedule);
        Human human1 = new Human("Ann", "Melnik", 1990, 23, scedule);
        Human human2 = new Human("Ann", "Melnik", 1990, 23, scedule);
        assertNotEquals(human.hashCode(),human1.hashCode());
        assertEquals(human1.hashCode(),human2.hashCode());
    }

    @Test
    void testToString() {
        String[][] scedule = {
                {DayOfWeek.SUNDAY.name(), "do home work"},
                {DayOfWeek.MONDAY.name(), "go to courses; watch a film;"},
                {DayOfWeek.TUESDAY.name(), "pass the English exam;"},
                {DayOfWeek.WEDNESDAY.name(), "go to the doctor;"},
                {DayOfWeek.THURSDAY.name(), "pay for utilities;"},
                {DayOfWeek.FRIDAY.name(), "write a resume;"},
                {DayOfWeek.SATURDAY.name(), "go have a beer with friends;"}
        };
        Human human = new Human("Anton", "Ivanov", 2003, 75, scedule);
        Human human1 = new Human("Ann", "Melnik", 1990, 23, scedule);
        String humanCheck = "Human{name='Anton', surname='Ivanov', year=2003, iq=75, schedule=[[SUNDAY, do home work]," +
                " [MONDAY, go to courses; watch a film;], [TUESDAY, pass the English exam;]," +
                " [WEDNESDAY, go to the doctor;], [THURSDAY, pay for utilities;]," +
                " [FRIDAY, write a resume;], [SATURDAY, go have a beer with friends;]]}";
        assertEquals(human.toString(), humanCheck);
        assertNotEquals(human1.toString(), humanCheck);
    }
}