package com.danIT.HW6;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {

    @Test
    void addChild() {
        Human child1 = new Human("Ivan", "Melnik", 2010);
        Human child2 = new Human("Sofi", "Melnik", 2018);
        Human child3 = new Human("Anton", "Ivanov", 2003);
        Human human1 = new Human("Ann", "Melnik", 1990);
        Human human2 = new Human("Ann", "Melnik", 1990);
        Family family1 = new Family(human1, human2, new Human[]{child2, child3});
        family1.addChild(child1);
        assertEquals(3, family1.getChildren().length);
        assertEquals(family1, child1.getFamily());
    }

    @Test
    void deleteChild() {
        Human child1 = new Human("Ivan", "Melnik", 2010);
        Human child2 = new Human("Sofi", "Melnik", 2018);
        Human child3 = new Human("Anton", "Ivanov", 2003);
        Human human1 = new Human("Ann", "Melnik", 1990);
        Human human2 = new Human("Ann", "Melnik", 1990);
        Family family1 = new Family(human1, human2, new Human[]{child1, child2, child3});
        family1.deleteChild(child3);
        assertEquals(2, family1.getChildren().length);
        assertEquals(null, child3.getFamily());
    }

    @Test
    void countFamily() {
        Human child1 = new Human("Ivan", "Melnik", 2010);
        Human child2 = new Human("Sofi", "Melnik", 2018);
        Human child3 = new Human("Anton", "Ivanov", 2003);
        Human human1 = new Human("Ann", "Melnik", 1990);
        Human human2 = new Human("Ann", "Melnik", 1990);
        Family family1 = new Family(human1, human2, new Human[]{child1, child2, child3});
        assertEquals(5, family1.countFamily());
    }

    @Test
    void testToString() {
        Human human = new Human("Anton", "Ivanov", 2003);
        Human human1 = new Human("Ann", "Melnik", 1990);
        Human human2 = new Human("Ann", "Melnik", 1990);
        Family family = new Family(human, human1);
        Family family1 = new Family(human1, human2);
        String familyCheck = "Family{mother=Human{name='Anton', surname='Ivanov', year=2003, " +
                "iq=0, schedule=[[null, null], [null, null], [null, null], [null, null], " +
                "[null, null], [null, null], [null, null]]}, father=Human{name='Ann', " +
                "surname='Melnik', year=1990, iq=0, schedule=[[null, null], [null, null], " +
                "[null, null], [null, null], [null, null], [null, null], [null, null]]}, " +
                "children=null, pet=null}";
        assertEquals(family.toString(), familyCheck);
        assertNotEquals(family1.toString(), familyCheck);
    }

    @Test
    void testEquals() {
        Human human = new Human("Anton", "Ivanov", 2003);
        Human human1 = new Human("Ann", "Melnik", 1990);
        Human human2 = new Human("Ann", "Melnik", 1990);
        Family family1 = new Family(human, human1);
        Family family2 = new Family(human1, human2);
        Family family3 = new Family(human1, human2);
        assertEquals(family2, family3);
        assertNotEquals(family1, family2);
    }

    @Test
    void testHashCode() {
        Human human = new Human("Anton", "Ivanov", 2003);
        Human human1 = new Human("Ann", "Melnik", 1990);
        Human human2 = new Human("Ann", "Melnik", 1990);
        Family family1 = new Family(human, human1);
        Family family2 = new Family(human1, human2);
        Family family3 = new Family(human1, human2);
        assertEquals(family2.hashCode(), family3.hashCode());
        assertNotEquals(family1.hashCode(), family2.hashCode());
    }
}