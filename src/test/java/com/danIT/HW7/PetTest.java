package com.danIT.HW7;


import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class PetTest {

    @Test
    void testToString() {
        Set<String> habits = new HashSet<>();
        habits.add("loves to play with the ball");
        habits.add("loves to chase a stick");

        Dog dog = new Dog(Species.DOG, "Barsik", 6, 60, habits);
        Fish fish = new Fish(Species.FISH, "Dori", 1, 10, habits);
        String petCheck = "Pet{species=DOG, nickname='Barsik', age=6, trickLevel=60, " +
                "habits=[loves to chase a stick, loves to play with the ball]}";
        assertEquals(dog.toString(), petCheck);
        assertNotEquals(fish.toString(), petCheck);
    }

    @Test
    void testEquals() {
       Set<String> habits = new HashSet<>();
        habits.add("loves to play with the ball");
        habits.add("loves to chase a stick");

        Dog dog1 = new Dog(Species.DOG, "Barsik", 6, 60, habits);
        Dog dog2 = new Dog(Species.DOG, "Barsik", 6, 60, habits);
        Fish fish = new Fish(Species.FISH, "Dori", 1, 10, habits);
        assertEquals(dog1, dog2);
        assertNotEquals(dog1, fish);
    }

    @Test
    void testHashCode() {
        Set<String> habits = new HashSet<>();
        habits.add("loves to play with the ball");
        habits.add("loves to chase a stick");

        Dog dog1 = new Dog(Species.DOG, "Barsik", 6, 60, habits);
        Dog dog2 = new Dog(Species.DOG, "Barsik", 6, 60, habits);
        Fish fish = new Fish(Species.FISH, "Dori", 1, 10, habits);
        assertEquals(dog1.hashCode(), dog2.hashCode());
        assertNotEquals(dog1.hashCode(), fish.hashCode());
    }
}