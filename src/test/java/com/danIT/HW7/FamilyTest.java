package com.danIT.HW7;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {

    @Test
    void addChild() {
       Human child1 = new Human("Ivan", "Melnik", 2010);
        Human child2 = new Human("Sofi", "Melnik", 2018);
        Human child3 = new Human("Anton", "Ivanov", 2003);
        Human human1 = new Human("Ann", "Melnik", 1990);
        Human human2 = new Human("Ann", "Melnik", 1990);
        List<Human> children = new ArrayList<>();
        children.add(child1);
        children.add(child2);
        Family family1 = new Family(human1, human2, children);
        family1.addChild(child3);
        assertEquals(3, family1.getChildren().size());
        assertEquals(family1, child3.getFamily());
    }

    @Test
    void deleteChild() {
        Human child1 = new Human("Ivan", "Melnik", 2010);
        Human child2 = new Human("Sofi", "Melnik", 2018);
        Human child3 = new Human("Anton", "Ivanov", 2003);
        Human human1 = new Human("Ann", "Melnik", 1990);
        Human human2 = new Human("Ann", "Melnik", 1990);
        List<Human> children = new ArrayList<>();
        children.add(child1);
        children.add(child2);
        Family family1 = new Family(human1, human2, children);
        family1.addChild(child3);
        family1.deleteChild(child3);
        assertEquals(2, family1.getChildren().size());
        assertEquals(null, child3.getFamily());
    }

    @Test
    void countFamily() {
        Human child1 = new Human("Ivan", "Melnik", 2010);
        Human child2 = new Human("Sofi", "Melnik", 2018);
        Human child3 = new Human("Anton", "Ivanov", 2003);
        Human human1 = new Human("Ann", "Melnik", 1990);
        Human human2 = new Human("Ann", "Melnik", 1990);
        List<Human> children = new ArrayList<>();
        children.add(child1);
        children.add(child2);
        Family family1 = new Family(human1, human2, children);
        family1.addChild(child3);
        assertEquals(5, family1.countFamily());
    }

    @Test
    void testToString() {
        Human human = new Human("Anton", "Ivanov", 2003);
        Human human1 = new Human("Ann", "Melnik", 1990);
        Human human2 = new Human("Ann", "Melnik", 1990);
        Family family1 = new Family(human1,human2);
        Family family2 = new Family(human1,human);
        String familyCheck = "Family{mother=Human{name='Ann', surname='Melnik', year=1990, iq=0, " +
                "schedule=null}, father=Human{name='Anton', surname='Ivanov', year=2003, iq=0, " +
                "schedule=null}, children=null, pets=null}";
        assertEquals(familyCheck,family2.toString());
        assertNotEquals(familyCheck,family1.toString());


    }

    @Test
    void testEquals() {
        Human human = new Human("Anton", "Ivanov", 2003);
        Human human1 = new Human("Ann", "Melnik", 1990);
        Human human2 = new Human("Ann", "Melnik", 1990);
        Family family1 = new Family(human1,human2);
        Family family2 = new Family(human1,human);
        Family family3 = new Family(human1,human);
        assertEquals(family2,family3);
        assertNotEquals(family1,family2);
    }

    @Test
    void testHashCode() {
        Human human = new Human("Anton", "Ivanov", 2003);
        Human human1 = new Human("Ann", "Melnik", 1990);
        Human human2 = new Human("Ann", "Melnik", 1990);
        Family family1 = new Family(human1,human2);
        Family family2 = new Family(human1,human);
        Family family3 = new Family(human1,human);
        assertEquals(family2.hashCode(),family3.hashCode());
        assertNotEquals(family1.hashCode(),family2.hashCode());
    }
}