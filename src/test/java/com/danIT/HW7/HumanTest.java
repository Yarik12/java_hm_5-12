package com.danIT.HW7;


import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {

    @Test
    void testEquals() {
        Map<DayOfWeek, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.SUNDAY, "do home work");
        schedule.put(DayOfWeek.MONDAY, "go to courses; watch a film;");
        schedule.put(DayOfWeek.TUESDAY, "pass the English exam;");
        schedule.put(DayOfWeek.WEDNESDAY, "go to the doctor;");
        schedule.put(DayOfWeek.THURSDAY, "pay for utilities;");
        schedule.put(DayOfWeek.FRIDAY, "write a resume;");
        schedule.put(DayOfWeek.SATURDAY, "go have a beer with friends;");

        Human human = new Human("Anton", "Ivanov", 2003, 75, schedule);
        Human human1 = new Human("Ann", "Melnik", 1990, 23, schedule);
        Human human2 = new Human("Ann", "Melnik", 1990, 23, schedule);
        assertNotEquals(human, human1);
        assertEquals(human1, human2);
    }

    @Test
    void testHashCode() {
        Map<DayOfWeek, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.SUNDAY, "do home work");
        schedule.put(DayOfWeek.MONDAY, "go to courses; watch a film;");
        schedule.put(DayOfWeek.TUESDAY, "pass the English exam;");
        schedule.put(DayOfWeek.WEDNESDAY, "go to the doctor;");
        schedule.put(DayOfWeek.THURSDAY, "pay for utilities;");
        schedule.put(DayOfWeek.FRIDAY, "write a resume;");
        schedule.put(DayOfWeek.SATURDAY, "go have a beer with friends;");

        Human human = new Human("Anton", "Ivanov", 2003, 75, schedule);
        Human human1 = new Human("Ann", "Melnik", 1990, 23, schedule);
        Human human2 = new Human("Ann", "Melnik", 1990, 23, schedule);
        assertNotEquals(human.hashCode(), human1.hashCode());
        assertEquals(human1.hashCode(), human2.hashCode());
    }

    @Test
    void testToString() {
        Map<DayOfWeek, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.SUNDAY, "do home work");
        schedule.put(DayOfWeek.MONDAY, "go to courses; watch a film;");
        schedule.put(DayOfWeek.TUESDAY, "pass the English exam;");
        schedule.put(DayOfWeek.WEDNESDAY, "go to the doctor;");
        schedule.put(DayOfWeek.THURSDAY, "pay for utilities;");
        schedule.put(DayOfWeek.FRIDAY, "write a resume;");
        schedule.put(DayOfWeek.SATURDAY, "go have a beer with friends;");

        Human human = new Human("Anton", "Ivanov", 2003, 75, schedule);
        Human human1 = new Human("Ann", "Melnik", 1990, 23, schedule);
        String humanCheck = "Human{name='Anton', surname='Ivanov', year=2003, iq=75, schedule={WEDNESDAY=go " +
                "to the doctor;, FRIDAY=write a resume;, SATURDAY=go have a beer with friends;, TUESDAY=pass " +
                "the English exam;, SUNDAY=do home work, THURSDAY=pay for utilities;, MONDAY=go to courses; " +
                "watch a film;}}";
        assertEquals(human.toString(), humanCheck);
        assertNotEquals(human1.toString(), humanCheck);
    }
}