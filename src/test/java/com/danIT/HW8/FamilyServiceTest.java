package com.danIT.HW8;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class FamilyServiceTest {

    @Test
    void getAllFamilies() {
        FamilyService familyService = new FamilyService();
        FamilyController familyController = new FamilyController(familyService);

        Human mother = new Human("Ann", "Melnik", 1990);
        Human father = new Human("Oleh", "Melnik", 1988);
        Human father1 = new Human("Ivan", "Metelitsa", 1980);
        Human mother1 = new Human("Ira", "Ivanova", 1990);
        familyController.createNewFamily(mother1, father1);
        familyController.createNewFamily(mother, father);

        List<Family> check = new ArrayList<>();
        check.add(new Family(mother1, father1));
        check.add(new Family(mother, father));

        assertEquals(check, familyController.getAllFamilies());
    }


    @Test
    void getFamiliesBiggerThan() {
        FamilyService familyService = new FamilyService();
        FamilyController familyController = new FamilyController(familyService);

        Human mother = new Human("Ann", "Melnik", 1990);
        Human father = new Human("Oleh", "Melnik", 1988);
        Human father1 = new Human("Ivan", "Metelitsa", 1980);
        Human mother1 = new Human("Ira", "Ivanova", 1990);
        familyController.createNewFamily(mother1, father1);
        familyController.createNewFamily(mother, father);

        List<Family> check = new ArrayList<>();
        check.add(new Family(mother1, father1));
        check.add(new Family(mother, father));

        assertEquals(check, familyController.getFamiliesBiggerThan(1));
    }

    @Test
    void getFamiliesLessThan() {
        FamilyService familyService = new FamilyService();
        FamilyController familyController = new FamilyController(familyService);

        Human mother = new Human("Ann", "Melnik", 1990);
        Human father = new Human("Oleh", "Melnik", 1988);
        Human father1 = new Human("Ivan", "Metelitsa", 1980);
        Human mother1 = new Human("Ira", "Ivanova", 1990);
        familyController.createNewFamily(mother1, father1);
        familyController.createNewFamily(mother, father);

        List<Family> check = new ArrayList<>();
        check.add(new Family(mother1, father1));
        check.add(new Family(mother, father));

        assertEquals(check, familyController.getFamiliesLessThan(3));
    }

    @Test
    void countFamiliesWithMemberNumber() {
        FamilyService familyService = new FamilyService();
        FamilyController familyController = new FamilyController(familyService);

        Human mother = new Human("Ann", "Melnik", 1990);
        Human father = new Human("Oleh", "Melnik", 1988);
        Human father1 = new Human("Ivan", "Metelitsa", 1980);
        Human mother1 = new Human("Ira", "Ivanova", 1990);
        familyController.createNewFamily(mother1, father1);
        familyController.createNewFamily(mother, father);

        assertEquals(2, familyController.countFamiliesWithMemberNumber(2));
    }


    @Test
    void deleteFamilyByIndex() {
        FamilyService familyService = new FamilyService();
        FamilyController familyController = new FamilyController(familyService);

        Human mother = new Human("Ann", "Melnik", 1990);
        Human father = new Human("Oleh", "Melnik", 1988);
        Human father1 = new Human("Ivan", "Metelitsa", 1980);
        Human mother1 = new Human("Ira", "Ivanova", 1990);
        familyController.createNewFamily(mother1, father1);
        familyController.createNewFamily(mother, father);

        familyController.deleteFamilyByIndex(1);

        assertEquals(1, familyController.getAllFamilies().size());
    }

    @Test
    void bornChild() {
        FamilyService familyService = new FamilyService();
        FamilyController familyController = new FamilyController(familyService);

        Human mother = new Human("Ann", "Melnik", 1990);
        Human father = new Human("Oleh", "Melnik", 1988);
        Human father1 = new Human("Ivan", "Metelitsa", 1980);
        Human mother1 = new Human("Ira", "Ivanova", 1990);
        familyController.createNewFamily(mother1, father1);
        familyController.createNewFamily(mother, father);

        familyController.bornChild(familyController.getFamilyById(1), NamesMan.JOHN, NamesWoman.LISA, 2024);

        assertEquals(1, familyController.getFamilyById(1).getChildren().size());
    }

    @Test
    void adoptChild() {
        FamilyService familyService = new FamilyService();
        FamilyController familyController = new FamilyController(familyService);

        Human mother = new Human("Ann", "Melnik", 1990);
        Human father = new Human("Oleh", "Melnik", 1988);
        Human father1 = new Human("Ivan", "Metelitsa", 1980);
        Human mother1 = new Human("Ira", "Ivanova", 1990);
        familyController.createNewFamily(mother1, father1);
        familyController.createNewFamily(mother, father);

        familyController.adoptChild(familyController.getFamilyById(1), new Human("Ivan", "Melnik", 2017));

        assertEquals(1, familyController.getFamilyById(1).getChildren().size());
    }

    @Test
    void deleteAllChildrenOlderThen() {

        Human child1 = new Human("Ivan", "Melnik", 2010);
        Human child2 = new Human("Sofi", "Melnik", 2018);
        Human child3 = new Human("Anton", "Ivanov", 2003);
        Human child4 = new Human("Anton", "Ivanov", 2005);
        Human child5 = new Human("Anton", "Ivanov", 2007);

        FamilyService familyService = new FamilyService();
        FamilyController familyController = new FamilyController(familyService);

        Human mother = new Human("Ann", "Melnik", 1990);
        Human father = new Human("Oleh", "Melnik", 1988);
        Human father1 = new Human("Ivan", "Metelitsa", 1980);
        Human mother1 = new Human("Ira", "Ivanova", 1990);
        familyController.createNewFamily(mother1, father1);
        familyController.createNewFamily(mother, father);

        familyController.getFamilyById(0).addChild(child1);
        familyController.getFamilyById(0).addChild(child2);
        familyController.getFamilyById(0).addChild(child3);
        familyController.getFamilyById(1).addChild(child4);
        familyController.getFamilyById(1).addChild(child5);

        familyController.deleteAllChildrenOlderThen(15, 2024);

        assertEquals(2, familyController.getFamilyById(0).getChildren().size());
        assertEquals(0, familyController.getFamilyById(1).getChildren().size());
    }

    @Test
    void count() {
        FamilyService familyService = new FamilyService();
        FamilyController familyController = new FamilyController(familyService);

        Human mother = new Human("Ann", "Melnik", 1990);
        Human father = new Human("Oleh", "Melnik", 1988);
        Human father1 = new Human("Ivan", "Metelitsa", 1980);
        Human mother1 = new Human("Ira", "Ivanova", 1990);
        familyController.createNewFamily(mother1, father1);
        familyController.createNewFamily(mother, father);

        assertEquals(2, familyController.count());
    }

    @Test
    void getFamilyById() {
        FamilyService familyService = new FamilyService();
        FamilyController familyController = new FamilyController(familyService);

        Human mother = new Human("Ann", "Melnik", 1990);
        Human father = new Human("Oleh", "Melnik", 1988);
        Human father1 = new Human("Ivan", "Metelitsa", 1980);
        Human mother1 = new Human("Ira", "Ivanova", 1990);
        familyController.createNewFamily(mother1, father1);
        familyController.createNewFamily(mother, father);

        Family familyCheck = new Family(mother1, father1);

        assertEquals(familyCheck,familyController.getFamilyById(0));
    }

    @Test
    void getPets() {
        FamilyService familyService = new FamilyService();
        FamilyController familyController = new FamilyController(familyService);

        Set<String> habits = new HashSet<>();
        habits.add("loves to play with the ball");
        habits.add("loves to chase a stick");

        Human father1 = new Human("Ivan", "Metelitsa", 1980);
        Human mother1 = new Human("Ira", "Ivanova", 1990);
        familyController.createNewFamily(mother1, father1);

        familyController.addPet(0, new Dog(Species.DOG, "Barsik", 6, 60, habits));
        Dog dog = new Dog(Species.DOG, "Barsik", 6, 60, habits);
        Set<Pet> check = new HashSet<>();
        check.add(dog);
        assertEquals(check, familyController.getPets(0));
    }

    @Test
    void addPet() {
        FamilyService familyService = new FamilyService();
        FamilyController familyController = new FamilyController(familyService);

        Set<String> habits = new HashSet<>();
        habits.add("loves to play with the ball");
        habits.add("loves to chase a stick");

        Human father1 = new Human("Ivan", "Metelitsa", 1980);
        Human mother1 = new Human("Ira", "Ivanova", 1990);
        familyController.createNewFamily(mother1, father1);

        familyController.addPet(0, new Dog(Species.DOG, "Barsik", 6, 60, habits));

        assertEquals(1, familyController.getPets(0).size());
    }
}