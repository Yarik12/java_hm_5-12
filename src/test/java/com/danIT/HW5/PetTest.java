package com.danIT.HW5;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class PetTest {

    @Test
    void testToString() {
        Pet pet = new Pet(Species.DOG, "Barsik", 6, 60, new String[]{"loves to play with the ball"
                , "loves to chase a stick"});
        Pet pet2 = new Pet(Species.DOG, "Bobik", 3, 40, new String[]{"loves to play with the ball"
                , "loves to chase a stick"});
        String petCheck = "species='DOG{, nickname='Barsik', age=6, trickLevel=60, habits=[loves to play with the ball," +
                " loves to chase a stick], canFly=false, numberOfLegs=4, hasFur=true}";
        assertEquals(pet.toString(), petCheck);
        assertNotEquals(pet2.toString(), petCheck);
    }

    @Test
    void testEquals() {
        Pet pet = new Pet(Species.DOG, "Barsik", 6, 60, new String[]{"loves to play with the ball"
                , "loves to chase a stick"});
        Pet pet2 = new Pet(Species.DOG, "Bobik", 3, 40, new String[]{"loves to play with the ball"
                , "loves to chase a stick"});
        Pet pet3 = new Pet(Species.DOG, "Bobik", 3, 40, new String[]{"loves to play with the ball"
                , "loves to chase a stick"});
        assertNotEquals(pet, pet2);
        assertEquals(pet2, pet3);

    }

    @Test
    void testHashCode() {
        Pet pet = new Pet(Species.DOG, "Barsik", 6, 60, new String[]{"loves to play with the ball"
                , "loves to chase a stick"});
        Pet pet2 = new Pet(Species.DOG, "Bobik", 3, 40, new String[]{"loves to play with the ball"
                , "loves to chase a stick"});
        Pet pet3 = new Pet(Species.DOG, "Bobik", 3, 40, new String[]{"loves to play with the ball"
                , "loves to chase a stick"});
        assertNotEquals(pet.hashCode(), pet2.hashCode());
        assertEquals(pet2.hashCode(), pet3.hashCode());
    }
}